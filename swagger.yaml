swagger: '2.0'
info:
  description: 'This service is used to retrieve and manage feature toggles'
  version: 1.0.0
  title: Feature Toggle API
host: '${domainName}'
basePath: '/${basePath}'
tags:
 - name: feature
schemes:
 - https

paths:
  /features/v1/{name}:
    get:
      summary: retrieve single feature 
      description: This operation returns the feature by that name if present
      operationId: getFeature
      tags: 
      - feature
      produces: 
      - application/json
      parameters:
        - name: name
          in: path
          required: true
          type: string
      responses:
        200:
          description: the feature exists and could be retrieved
          schema:
            $ref: '#/definitions/Feature'
        404:
          description: the feature was not found
          schema:
            $ref: '#/definitions/ErrorMessage'
        500:
          description: internal server error
          schema:
            $ref: '#/definitions/ErrorMessage'
    delete:
      summary: delete single feature 
      description: This operation deletes the feature by that name if present
      operationId: deleteFeature
      tags: 
      - feature
      produces: 
      - application/json
      parameters:
        - name: name
          in: path
          required: true
          type: string
      responses:
        200:
          description: the feature exists and could be deleted
          schema:
            $ref: '#/definitions/Feature'
        404:
          description: the feature was not found
          schema:
            $ref: '#/definitions/ErrorMessage'
        500:
          description: internal server error
          schema:
            $ref: '#/definitions/ErrorMessage'
            
  /features/v1/:
    post:
      summary: create new feature 
      description: This operation is used to create a new feature
      operationId: createFeature
      tags:
      - feature
      produces: 
      - application/json
      parameters: 
        - name: feature
          in: body
          required: true
          schema:
            $ref: '#/definitions/CreateFeatureRequest'
      responses:
        201:
          description: the feature is successfully created
          schema:
            $ref: '#/definitions/Feature'
        400:
          description: bad request, the feature already exists
          schema:
            $ref: '#/definitions/ErrorMessage'
        500:
          description: internal server error
          schema:
            $ref: '#/definitions/ErrorMessage'
            
    put:
      summary: update feature 
      description: This operation is used to update an existing feature
      operationId: updateFeature
      tags: 
      - feature
      produces: 
      - application/json
      parameters: 
        - name: feature
          in: body
          required: true
          schema:
            $ref: '#/definitions/UpdateFeatureRequest'
      responses:
        201:
          description: the feature is successfully updated
          schema:
            $ref: '#/definitions/Feature'
        400:
          description: bad request, the feature does not exist
          schema:
            $ref: '#/definitions/ErrorMessage'
        500:
          description: internal server error
          schema:
            $ref: '#/definitions/ErrorMessage'
    
definitions:
  CreateFeatureRequest:
    type: object
    required:
    - name
    - description
    - isEnabled
    properties:
      name:
        type: string
        description: unique name of the feature
      description:
        type: string
        description: description of the feature
      isEnabled:
        type: boolean
        description: status of the toggle
  UpdateFeatureRequest:
    type: object
    required:
    - name
    properties:
      name:
        type: string
        description: unique name of the feature
      description:
        type: string
        description: description of the feature
      isEnabled:
        type: boolean
        description: status of the toggle
  Feature:
    type: object
    properties:
      name:
        type: string
        description: unique name of the feature
      description:
        type: string
        description: description of the feature
      isEnabled:
        type: boolean
        description: status of the toggle
  ErrorMessage:
    type: object
    properties:
      message:
        type: string
        description: description of the error
      statusCode:
        type: integer
        description: http status code