package com.nike.lambda.exception;

public class FeatureDoesNotExistException extends IllegalArgumentException {
    public FeatureDoesNotExistException(String message) {
        super(message);
    }
}
