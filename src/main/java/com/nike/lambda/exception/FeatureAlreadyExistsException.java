package com.nike.lambda.exception;

public class FeatureAlreadyExistsException extends IllegalArgumentException {
    public FeatureAlreadyExistsException(String message) {
        super(message);
    }
}
