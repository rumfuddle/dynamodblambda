package com.nike.lambda.handler;

import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nike.lambda.model.response.ErrorMessage;
import com.nike.lambda.model.response.GatewayResponse;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Map;

public interface FeatureRequestStreamHandler extends RequestStreamHandler {
    int SC_OK = 200;
    int SC_CREATED = 201;
    int SC_BAD_REQUEST = 400;
    int SC_NOT_FOUND = 404;
    int SC_INTERNAL_SERVER_ERROR = 500;
    Map<String, String> APPLICATION_JSON = Collections.singletonMap("Content-Type", "application/json");
    ErrorMessage REQUEST_WAS_NULL_ERROR = new ErrorMessage("request was null", SC_BAD_REQUEST);
    ErrorMessage FEATURE_NAME_WAS_NOT_SET = new ErrorMessage("feature name was not set", SC_BAD_REQUEST);
    ErrorMessage FEATURE_TABLE_DOES_NOT_EXIST = new ErrorMessage("feature table does not exist", SC_INTERNAL_SERVER_ERROR);
    ErrorMessage FEATURE_DOES_NOT_EXIST = new ErrorMessage("feature does not exist", SC_BAD_REQUEST);
    ErrorMessage FEATURE_ALREADY_EXISTS = new ErrorMessage("feature already exists", SC_BAD_REQUEST);


    default void writeInvalidJsonInStreamResponse(ObjectMapper objectMapper, OutputStream outputStream, String details) throws IOException {
        objectMapper.writeValue(
                outputStream, new GatewayResponse<>(
                        objectMapper.writeValueAsString(new ErrorMessage("invalid JSON in body: " + details, SC_BAD_REQUEST)),
                        APPLICATION_JSON,
                        SC_BAD_REQUEST
                )
        );
    }

    default boolean isNullOrEmpty(String string) {
        return string == null || string.isEmpty();
    }
}
