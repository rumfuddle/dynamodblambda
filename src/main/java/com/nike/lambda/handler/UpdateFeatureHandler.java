package com.nike.lambda.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nike.lambda.model.Feature;
import lombok.extern.slf4j.Slf4j;
import com.nike.lambda.config.DaggerFeatureComponent;
import com.nike.lambda.config.FeatureComponent;
import com.nike.lambda.dao.FeatureDao;
import com.nike.lambda.exception.FeatureDoesNotExistException;
import com.nike.lambda.exception.TableDoesNotExistException;
import com.nike.lambda.model.request.UpdateFeatureRequest;
import com.nike.lambda.model.response.ErrorMessage;
import com.nike.lambda.model.response.GatewayResponse;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Slf4j
public class UpdateFeatureHandler implements FeatureRequestStreamHandler {
    @Inject
    ObjectMapper objectMapper;
    @Inject
    FeatureDao featureDao;
    private final FeatureComponent featureComponent;

    public UpdateFeatureHandler() {
        featureComponent = DaggerFeatureComponent.builder().build();
        featureComponent.inject(this);
    }

    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {
        final JsonNode event;

        try {
            event = objectMapper.readTree(inputStream);
        } catch (JsonProcessingException e) {
            writeInvalidJsonInStreamResponse(objectMapper, outputStream, e.getMessage());
            return;
        }

        if (event == null) {
            writeInvalidJsonInStreamResponse(objectMapper, outputStream, "event was null");
            return;
        }

        JsonNode updateFeatureRequestBody = event.findValue("body");

        if (updateFeatureRequestBody == null) {
            objectMapper.writeValue(
                    outputStream,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(
                                    new ErrorMessage("body was null", SC_BAD_REQUEST)
                            ),
                            APPLICATION_JSON,
                            SC_BAD_REQUEST
                    )
            );
            return;
        }

        UpdateFeatureRequest updateFeatureRequest;
        try {
            updateFeatureRequest = objectMapper.treeToValue(
                    objectMapper.readTree(updateFeatureRequestBody.asText()),
                    UpdateFeatureRequest.class
            );
        } catch (JsonProcessingException e) {
            objectMapper.writeValue(
                    outputStream,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(
                                    new ErrorMessage("invalid JSON in body: " + e.getMessage(), SC_BAD_REQUEST)
                            ),
                            APPLICATION_JSON,
                            SC_BAD_REQUEST
                    )
            );
            return;
        }

        if (updateFeatureRequest == null) {
            objectMapper.writeValue(
                    outputStream,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(REQUEST_WAS_NULL_ERROR),
                            APPLICATION_JSON,
                            SC_BAD_REQUEST
                    )
            );
            return;
        }

        if (isNullOrEmpty(updateFeatureRequest.getName())) {
            objectMapper.writeValue(
                    outputStream,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(FEATURE_NAME_WAS_NOT_SET),
                            APPLICATION_JSON,
                            SC_BAD_REQUEST
                    )
            );
            return;
        }

        log.info("updating feature '{}'", updateFeatureRequest.getName());

        try {
            Feature feature = featureDao.updateFeature(updateFeatureRequest.convert());
            log.info("updated {}", feature);
            objectMapper.writeValue(
                    outputStream,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(feature),
                            APPLICATION_JSON,
                            SC_OK
                    )
            );
        } catch (TableDoesNotExistException e) {
            log.error("feature table does not exist");
            objectMapper.writeValue(
                    outputStream,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(FEATURE_TABLE_DOES_NOT_EXIST),
                            APPLICATION_JSON,
                            SC_INTERNAL_SERVER_ERROR
                    )
            );
        } catch (FeatureDoesNotExistException e) {
            log.error("feature {} does not exist", updateFeatureRequest.getName());
            objectMapper.writeValue(
                    outputStream,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(FEATURE_DOES_NOT_EXIST),
                            APPLICATION_JSON,
                            SC_BAD_REQUEST
                    )
            );
        } catch (Exception e) {
            log.error("feature update failed: {}", e.getMessage());
            objectMapper.writeValue(
                    outputStream,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(new ErrorMessage(
                                    "feature update failed: " + e.getMessage(),
                                    SC_INTERNAL_SERVER_ERROR
                            )),
                            APPLICATION_JSON,
                            SC_INTERNAL_SERVER_ERROR
                    )
            );
        }
    }
}
