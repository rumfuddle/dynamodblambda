package com.nike.lambda.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nike.lambda.config.DaggerFeatureComponent;
import com.nike.lambda.config.FeatureComponent;
import com.nike.lambda.exception.FeatureDoesNotExistException;
import com.nike.lambda.exception.TableDoesNotExistException;
import com.nike.lambda.model.Feature;
import com.nike.lambda.model.response.GatewayResponse;
import lombok.extern.slf4j.Slf4j;
import com.nike.lambda.dao.FeatureDao;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Optional;

@Slf4j
public class GetFeatureHandler implements FeatureRequestStreamHandler {
    @Inject
    ObjectMapper objectMapper;
    @Inject
    FeatureDao featureDao;
    private final FeatureComponent featureComponent;

    public GetFeatureHandler() {
        featureComponent = DaggerFeatureComponent.builder().build();
        featureComponent.inject(this);
    }

    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {
        final JsonNode event;

        try {
            event = objectMapper.readTree(inputStream);
        } catch (JsonProcessingException e) {
            writeInvalidJsonInStreamResponse(objectMapper, outputStream, e.getMessage());
            return;
        }

        if (event == null) {
            writeInvalidJsonInStreamResponse(objectMapper, outputStream, "event was null");
            return;
        }

        JsonNode pathParameterMap = event.findValue("pathParameters");

        String featureName = Optional.ofNullable(pathParameterMap)
                .map(mapNode -> mapNode.get("feature_name"))
                .map(JsonNode::asText)
                .orElse(null);

        if (isNullOrEmpty(featureName)) {
            log.error("missing feature name");
            objectMapper.writeValue(
                    outputStream,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(FEATURE_NAME_WAS_NOT_SET),
                            APPLICATION_JSON,
                            SC_BAD_REQUEST
                    )
            );
            return;
        }

        log.info("retrieving feature '{}'", featureName);

        try {
            Feature feature = featureDao.getFeature(featureName);
            log.info("retrieved {}", feature);
            objectMapper.writeValue(
                    outputStream,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(feature),
                            APPLICATION_JSON,
                            SC_OK
                    )
            );
        } catch (FeatureDoesNotExistException e) {
            log.error("feature '{}' does not exist", featureName);
            objectMapper.writeValue(
                    outputStream,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(FEATURE_DOES_NOT_EXIST),
                            APPLICATION_JSON,
                            SC_NOT_FOUND
                    )
            );
        } catch (TableDoesNotExistException e) {
            log.error("feature table does not exist");
            objectMapper.writeValue(
                    outputStream,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(FEATURE_TABLE_DOES_NOT_EXIST),
                            APPLICATION_JSON,
                            SC_INTERNAL_SERVER_ERROR
                    )
            );
        }
    }
}
