package com.nike.lambda.dao;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDeleteExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBSaveExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.ExpectedAttributeValue;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.nike.lambda.exception.FeatureAlreadyExistsException;
import com.nike.lambda.exception.FeatureDoesNotExistException;
import com.nike.lambda.exception.TableDoesNotExistException;
import com.nike.lambda.model.Feature;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class FeatureDao {
    private final AmazonDynamoDB dynamoDBClient;

    public FeatureDao(AmazonDynamoDB dynamoDBClient) {
        this.dynamoDBClient = dynamoDBClient;
    }

    public Feature getFeature(String featureName) {
        try {
            return Optional.ofNullable(
                    getMapper().load(Feature.class, featureName)
            ).orElseThrow(() -> new FeatureDoesNotExistException("feature " + featureName + " does not exist"));
        } catch (ResourceNotFoundException e) {
            throw new TableDoesNotExistException("feature table does not exist");
        }
    }

    public Feature createFeature(Feature feature) {
        try {
            DynamoDBSaveExpression saveExpression = new DynamoDBSaveExpression();
            Map<String, ExpectedAttributeValue> expected = new HashMap<>();
            expected.put("name", new ExpectedAttributeValue(false));
            saveExpression.setExpected(expected);

            getMapper().save(feature, saveExpression);
            return feature;
        } catch (ResourceNotFoundException e) {
            throw new TableDoesNotExistException("feature table does not exist");
        } catch (ConditionalCheckFailedException e) {
            throw new FeatureAlreadyExistsException("feature " + feature.getName() + " already exists");
        }
    }

    public Feature updateFeature(Feature feature) {
        try {
            DynamoDBSaveExpression saveExpression = new DynamoDBSaveExpression();
            Map<String, ExpectedAttributeValue> expected = new HashMap<>();
            expected.put("name", new ExpectedAttributeValue(new AttributeValue(feature.getName())));
            saveExpression.setExpected(expected);

            getMapper().save(feature, saveExpression);
            return getFeature(feature.getName());
        } catch (ResourceNotFoundException e) {
            throw new TableDoesNotExistException("feature table does not exist");
        } catch (ConditionalCheckFailedException e) {
            throw new FeatureDoesNotExistException("feature " + feature.getName() + " does not exist");
        }
    }

    public Feature deleteFeature(Feature feature) {
        try {
            DynamoDBDeleteExpression deleteExpression = new DynamoDBDeleteExpression();
            Map<String, ExpectedAttributeValue> expected = new HashMap<>();
            expected.put("name", new ExpectedAttributeValue(new AttributeValue(feature.getName())));
            deleteExpression.setExpected(expected);

            getMapper().delete(feature, deleteExpression);
            return feature;
        } catch (ResourceNotFoundException e) {
            throw new TableDoesNotExistException("feature table does not exist");
        } catch (ConditionalCheckFailedException e) {
            throw new FeatureDoesNotExistException("feature " + feature.getName() + " does not exist");
        }
    }

    private DynamoDBMapper getMapper() {
        DynamoDBMapperConfig mapperConfig = DynamoDBMapperConfig
                .builder()
                .withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.UPDATE_SKIP_NULL_ATTRIBUTES)
                .build();
        return new DynamoDBMapper(dynamoDBClient, mapperConfig);
    }
}
