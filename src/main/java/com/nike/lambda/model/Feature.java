package com.nike.lambda.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@DynamoDBTable(tableName = "features_table")
public class Feature {
    @DynamoDBHashKey
    private String name;
    @DynamoDBAttribute
    private String description;
    @DynamoDBAttribute
    private Boolean isEnabled;

    @Override
    public String toString() {
        return "feature '" + name + "':" +
                " description='" + description + '\'' +
                ", isEnabled=" + isEnabled;
    }
}
