package com.nike.lambda.model.request;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.nike.lambda.model.Feature;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonAutoDetect
public class CreateFeatureRequest {
    private String name;
    private String description;
    private Boolean isEnabled;

    public Feature convert() {
        return new Feature(name, description, isEnabled);
    }
}
