package com.nike.lambda.model.response;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Getter;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Getter
@JsonAutoDetect
public class GatewayResponse<T> {
    private final T body;
    private final Map<String, String> headers;
    private final int statusCode;

    public GatewayResponse(T body, Map<String, String> headers, int statusCode) {
        this.body = body;
        this.headers = Collections.unmodifiableMap(new HashMap<>(headers));
        this.statusCode = statusCode;
    }
}
