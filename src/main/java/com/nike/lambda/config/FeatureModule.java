package com.nike.lambda.config;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import dagger.Module;
import dagger.Provides;
import com.nike.lambda.dao.FeatureDao;

import javax.inject.Singleton;

@Module
public class FeatureModule {
    @Singleton
    @Provides
    ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Singleton
    @Provides
    AmazonDynamoDB dynamoDBClient() {
        String endpoint = System.getenv("ENDPOINT_OVERRIDE");

        AmazonDynamoDBClientBuilder builder = AmazonDynamoDBClientBuilder.standard();
        if (endpoint != null && !endpoint.isEmpty()) {
            builder.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint, "eu-central-1"));
        }

        return builder.build();
    }

    @Singleton
    @Provides
    public FeatureDao featureDao(AmazonDynamoDB dynamoDBClient) {
        return new FeatureDao(dynamoDBClient);
    }
}
