package com.nike.lambda.config;

import com.nike.lambda.handler.DeleteFeatureHandler;
import com.nike.lambda.handler.UpdateFeatureHandler;
import dagger.Component;
import com.nike.lambda.dao.FeatureDao;
import com.nike.lambda.handler.CreateFeatureHandler;
import com.nike.lambda.handler.GetFeatureHandler;

import javax.inject.Singleton;

@Singleton
@Component(modules = {FeatureModule.class})
public interface FeatureComponent {
    FeatureDao provideFeatureDao();

    void inject(GetFeatureHandler requestHandler);

    void inject(CreateFeatureHandler requestHandler);

    void inject(DeleteFeatureHandler requestHandler);

    void inject(UpdateFeatureHandler requestHandler);
}
