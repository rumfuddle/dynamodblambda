package com.nike.lambda.services.lambda.runtime;

import com.nike.lambda.model.Feature;

public class TestFeature extends Feature {
    public TestFeature() {
        super("test", "this is a test", true);
    }

    public String convertToBody() {
        return String.format(
                "{\"body\": \"{\\\"name\\\": \\\"%s\\\", \\\"description\\\":\\\"%s\\\", \\\"isEnabled\\\": %b}\"}",
                getName(),
                getDescription(),
                getIsEnabled()
        );
    }

    public String convertToPathParameters() {
        return String.format(
                "{\"pathParameters\": {\"feature_name\": \"%s\"}}",
                getName()
        );
    }
}
