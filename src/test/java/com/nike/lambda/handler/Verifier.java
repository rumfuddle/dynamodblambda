package com.nike.lambda.handler;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.nike.lambda.model.Feature;

import java.util.Map;

import static org.junit.Assert.*;

public class Verifier {
    public static void verifyHeaders(Item item) {
        assertTrue("no headers", item.hasAttribute("headers"));
        Map<String, Object> headers = item.getMap("headers");
        assertNotNull("headers is null", headers);
        assertEquals("too many headers", 1, headers.size());
        assertTrue("missing content type header", headers.containsKey("Content-Type"));
    }

    public static void verifyStatusCode(Item item, int statusCode) {
        assertTrue("missing status code", item.hasAttribute("statusCode"));
        assertEquals("wrong status code", statusCode, item.getInt("statusCode"));
    }

    public static void verifyFeature(Item item, Feature feature) {
        assertTrue("missing body", item.hasAttribute("body"));
        String bodyString = item.getString("body");
        assertNotNull("body is null", bodyString);
        Item body = Item.fromJSON(bodyString);

        assertTrue("missing name", body.hasAttribute("name"));
        String name = body.getString("name");
        assertNotNull("name is null", name);
        assertEquals("wrong name", feature.getName(), name);

        assertTrue("missing description", body.hasAttribute("description"));
        String description = body.getString("description");
        assertNotNull("description is null", description);
        assertEquals("wrong description", feature.getDescription(), description);

        assertTrue("missing isEnabled", body.hasAttribute("isEnabled"));
        Boolean isEnabled = body.getBoolean("isEnabled");
        assertNotNull("isEnabled is null", isEnabled);
        assertEquals("wrong isEnabled", feature.getIsEnabled(), isEnabled);
    }
}
