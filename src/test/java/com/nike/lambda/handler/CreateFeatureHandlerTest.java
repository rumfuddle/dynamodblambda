package com.nike.lambda.handler;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.lambda.runtime.Context;
import com.nike.lambda.services.lambda.runtime.TestContext;
import com.nike.lambda.services.lambda.runtime.TestFeature;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;


public class CreateFeatureHandlerTest {
    private final CreateFeatureHandler createFeatureHandler = new CreateFeatureHandler();
    private final DeleteFeatureHandler deleteFeatureHandler = new DeleteFeatureHandler();
    private final GetFeatureHandler getFeatureHandler = new GetFeatureHandler();
    private final UpdateFeatureHandler updateFeatureHandler = new UpdateFeatureHandler();
    private final TestFeature testFeature = new TestFeature();

    @Before
    @After
    public void cleanup() throws IOException {
        Context context = TestContext.builder().build();
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        String input = testFeature.convertToPathParameters();

        deleteFeatureHandler.handleRequest(new ByteArrayInputStream(input.getBytes()), os, context);
    }

    @Test
    public void handleRequest_whenCreateFeatureInputStreamOk_puts200InOutputStream() throws IOException {
        Context context = TestContext.builder().build();
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        String input = testFeature.convertToBody();

        createFeatureHandler.handleRequest(new ByteArrayInputStream(input.getBytes()), os, context);

        Item item = Item.fromJSON(os.toString());

        Verifier.verifyHeaders(item);
        Verifier.verifyStatusCode(item, 201);
        Verifier.verifyFeature(item, testFeature);

        input = testFeature.convertToPathParameters();
        os = new ByteArrayOutputStream();

        getFeatureHandler.handleRequest(new ByteArrayInputStream(input.getBytes()), os, context);

        item = Item.fromJSON(os.toString());

        Verifier.verifyHeaders(item);
        Verifier.verifyStatusCode(item, 200);
        Verifier.verifyFeature(item, testFeature);

        testFeature.setIsEnabled(false);

        input = testFeature.convertToBody();
        os = new ByteArrayOutputStream();

        updateFeatureHandler.handleRequest(new ByteArrayInputStream(input.getBytes()), os, context);

        input = testFeature.convertToPathParameters();
        os = new ByteArrayOutputStream();

        getFeatureHandler.handleRequest(new ByteArrayInputStream(input.getBytes()), os, context);

        item = Item.fromJSON(os.toString());

        Verifier.verifyHeaders(item);
        Verifier.verifyStatusCode(item, 200);
        Verifier.verifyFeature(item, testFeature);
    }
}
