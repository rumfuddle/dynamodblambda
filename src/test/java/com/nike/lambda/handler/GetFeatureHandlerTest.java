package com.nike.lambda.handler;

import com.nike.lambda.services.lambda.runtime.TestContext;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.junit.Assert.assertTrue;

public class GetFeatureHandlerTest {
    private GetFeatureHandler getFeatureHandler = new GetFeatureHandler();


    @Test
    public void handleRequest_whenGetFeatureInputStreamEmpty_puts400InOutputStream() throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        getFeatureHandler.handleRequest(new ByteArrayInputStream(new byte[0]), os, TestContext.builder().build());
        assertTrue(os.toString().contains("invalid JSON"));
        assertTrue(os.toString().contains("400"));
    }
}
